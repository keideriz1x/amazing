-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-10-2020 a las 21:57:33
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `amazonasbd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Administrador`
--

CREATE TABLE `Administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Administrador`
--

INSERT INTO `Administrador` (`idAdministrador`, `nombre`, `apellido`, `correo`, `clave`) VALUES
(1, 'Clark', 'Kent', '123@123.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Autor`
--

CREATE TABLE `Autor` (
  `idAutor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Autor`
--

INSERT INTO `Autor` (`idAutor`, `nombre`, `apellido`) VALUES
(1, 'Gabriel', 'Garcia Marquez'),
(2, 'Mario', 'Vargas Llosa'),
(3, 'Paulo', 'Cohelo'),
(4, 'Milan ', 'Kundera'),
(5, 'Hector ', 'Abad Faciolince'),
(6, 'Charles ', 'Dickens'),
(7, 'Oscar ', 'Wilde'),
(8, 'Franz ', 'Kafka'),
(9, 'Julio ', 'Cortazar'),
(10, 'Edgar', 'Allan Poe'),
(11, 'Viktor ', 'Frankl'),
(12, 'José ', 'Asunción Silva'),
(13, 'Gonzalo ', 'Arango'),
(14, 'Arthur ', 'Rimbaud'),
(15, 'Stephen ', 'King'),
(17, 'Virginia ', 'Wolff');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Carrito`
--

CREATE TABLE `Carrito` (
  `idCarrito` int(11) NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Carrito_Libro`
--

CREATE TABLE `Carrito_Libro` (
  `idCarrito_Libro` int(11) NOT NULL,
  `cantidad` varchar(45) NOT NULL,
  `Carrito_idCarrito` int(11) NOT NULL,
  `Libro_idLibro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cliente`
--

CREATE TABLE `Cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Libro`
--

CREATE TABLE `Libro` (
  `idLibro` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `paginas` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `Autor_idAutor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `LogAdministrador`
--

CREATE TABLE `LogAdministrador` (
  `idLogAdministrador` int(11) NOT NULL,
  `accion` varchar(45) NOT NULL,
  `datos` text NOT NULL,
  `fecha` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `Administrador_idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Administrador`
--
ALTER TABLE `Administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `Autor`
--
ALTER TABLE `Autor`
  ADD PRIMARY KEY (`idAutor`);

--
-- Indices de la tabla `Carrito`
--
ALTER TABLE `Carrito`
  ADD PRIMARY KEY (`idCarrito`,`Cliente_idCliente`),
  ADD KEY `fk_Carrito_Cliente1_idx` (`Cliente_idCliente`);

--
-- Indices de la tabla `Carrito_Libro`
--
ALTER TABLE `Carrito_Libro`
  ADD PRIMARY KEY (`idCarrito_Libro`,`Carrito_idCarrito`,`Libro_idLibro`),
  ADD KEY `fk_Carrito_Libro_Carrito1_idx` (`Carrito_idCarrito`),
  ADD KEY `fk_Carrito_Libro_Libro1_idx` (`Libro_idLibro`);

--
-- Indices de la tabla `Cliente`
--
ALTER TABLE `Cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `Libro`
--
ALTER TABLE `Libro`
  ADD PRIMARY KEY (`idLibro`,`Autor_idAutor`),
  ADD KEY `fk_Libro_Autor_idx` (`Autor_idAutor`);

--
-- Indices de la tabla `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  ADD PRIMARY KEY (`idLogAdministrador`,`Administrador_idAdministrador`),
  ADD KEY `fk_LogAdministrador_Administrador1_idx` (`Administrador_idAdministrador`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Administrador`
--
ALTER TABLE `Administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `Autor`
--
ALTER TABLE `Autor`
  MODIFY `idAutor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `Carrito`
--
ALTER TABLE `Carrito`
  MODIFY `idCarrito` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Carrito_Libro`
--
ALTER TABLE `Carrito_Libro`
  MODIFY `idCarrito_Libro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Cliente`
--
ALTER TABLE `Cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `Libro`
--
ALTER TABLE `Libro`
  MODIFY `idLibro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  MODIFY `idLogAdministrador` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Carrito`
--
ALTER TABLE `Carrito`
  ADD CONSTRAINT `fk_Carrito_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `Cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Carrito_Libro`
--
ALTER TABLE `Carrito_Libro`
  ADD CONSTRAINT `fk_Carrito_Libro_Carrito1` FOREIGN KEY (`Carrito_idCarrito`) REFERENCES `Carrito` (`idCarrito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Carrito_Libro_Libro1` FOREIGN KEY (`Libro_idLibro`) REFERENCES `Libro` (`idLibro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Libro`
--
ALTER TABLE `Libro`
  ADD CONSTRAINT `fk_Libro_Autor` FOREIGN KEY (`Autor_idAutor`) REFERENCES `Autor` (`idAutor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `LogAdministrador`
--
ALTER TABLE `LogAdministrador`
  ADD CONSTRAINT `fk_LogAdministrador_Administrador1` FOREIGN KEY (`Administrador_idAdministrador`) REFERENCES `Administrador` (`idAdministrador`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
