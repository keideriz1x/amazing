<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
<title>Amazing</title>
<link rel="icon" type="image/png" href="img/logo.png">
</head>
<body>
	<div class="container">
		<div class="row mt-3">
			<div class="col-3 text-center">
				<img src="img/logo.png" width="100px">
			</div>
			<div class="col-9 text-center">
				<h1>Amazing</h1>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-8">
				<div class="card">
					<div class="card-header">
						<h3>Proyecto Amazing</h3>
					</div>
					<div class="card-body">
						<img src="img/1.jpg" width="100%">
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="card">
					<div class="card-header">
						<h3>Autenticación</h3>
					</div>
					<div class="card-body">
						<form>
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Correo" required="required">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" placeholder="Contraseña" required="required">
							</div>
							<button type="submit" class="btn btn-primary btn-block">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>